// Copyright 2020 Ian Jackson
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

use std::ops::Deref;
use std::fmt::{Display,Debug,Formatter,self};

pub struct Immutable<T> (T);
impl<T> Immutable<T> {
  pub fn new(t : T) -> Immutable<T> { Immutable(t) }
}
impl<T> Deref for Immutable<T> {
  type Target = T;
  fn deref(&self) -> &T { &self.0 }
}
impl<T> From<T> for Immutable<T> {
  fn from(t : T) -> Immutable<T> { Immutable(t) }
}

impl<T : Display> Display for Immutable<T> {
  fn fmt(&self, fmt : &mut Formatter) -> Result<(),fmt::Error> {
    self.0.fmt(fmt)
  }
}
impl<T : Debug> Debug for Immutable<T> {
  fn fmt(&self, fmt : &mut Formatter) -> Result<(),fmt::Error> {
    self.0.fmt(fmt)
  }
}
