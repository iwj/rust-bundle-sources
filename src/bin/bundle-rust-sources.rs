// Copyright 2020 Ian Jackson
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

// rustcargo --E env target/debug/bundle-rust-sources --output /home/rustcargo/Rustup/Tau/bundle-sources/e --manifest-path=`pwd`

use bundle_sources::*;

type E = anyhow::Error;

#[derive(Debug,clap::Parser)]
struct Opts {
  /// Output directory, will be overwritten
  #[structopt(short,long)]
  output : String,

  /// Paths to git directories (\[outleafname:\]path)
  #[structopt(long,short='g')]
  git : Vec<String>,

  /// Paths to raw directories (\[outleafname:\]path)
  #[structopt(long,short='R')]
  raw : Vec<String>,

  /// Paths to individual files (\[outfilename:\]path)
  #[structopt(long,short)]
  file : Vec<String>,

  /// Path to `Cargo.toml`\[s\] (or the directories containing them)
  #[structopt(long,short='M')]
  manifest_path : Vec<String>,

  /// Do not delete intermediate sub-tarballs etc.
  // https://github.com/TeXitoi/structopt/issues/104
  #[structopt(long)]
  no_tidy : bool,
}

fn main() -> Result<(),E> {
  let opts = <Opts as clap::Parser>::parse();

  let mut config = Config::new();
  if opts.no_tidy                     { config.tidy(false);            }

  let mut sg = SourceGenerator::new_trashing(opts.output.clone())?;
  sg.configure(config);

  if opts.manifest_path.is_empty() {
    sg.add_cargo_packages(None)?;
  }

  {
    fn add_comp<C : Component>(sg : &mut SourceGenerator,
                src : &str, comp_map : fn(String) -> C)
                -> Result<(),E> {
      let leaf = src.rsplitn(2,'/').next().unwrap();
      let (ident, src) = if let Some(colon) = src.find(':') {
        let (l, r) = src.split_at(colon);
        (l, &r[1..])
      } else {
        (leaf, src)
      };
      let comp = comp_map(src.to_owned());
      let desc = comp.default_desc();
      sg.add_component(&comp, ident.clone(), Category::Mixed, desc)
    }

    for g in &opts.git { add_comp(&mut sg, g, GitComponent         )?; }
    for g in &opts.raw { add_comp(&mut sg, g, RawDirectoryComponent)?; }
    for g in &opts.file { add_comp(&mut sg, g, FileComponent        )?; }
  }

  for m in &opts.manifest_path {
    sg.add_cargo_packages(Some(m))?;
  }

  sg.aggregate()?;
  Ok(())
}
