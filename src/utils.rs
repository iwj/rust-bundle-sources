// Copyright 2020 Ian Jackson
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

use crate::imports::*;

type E = anyhow::Error;

#[throws(E)]
pub fn check_exit_status(st : ExitStatus) {
  if !st.success() {
    throw!(anyhow!("program failed: {:?}",st))
  }
}

#[throws(E)]
pub fn run_cmd(mut cmd : Command) {
  let st = cmd.status()?;
  check_exit_status(st)?;
}

pub fn tar_command() -> Command {
  let mut cmd = Command::new("tar");
  cmd.args(&["--owner=root:0","--group=root:0","--mode=ug=rwX,o=rX"]);
  cmd
}

pub trait ErrorMap<T,E0> {
  type R;
  fn cxm<S,MF>(self, mf : MF) -> Self::R
  where MF : FnOnce() -> S,
        S : ToOwned<Owned=String>,
        String : Borrow<S>;
}

impl<T,E0> ErrorMap<T,E0> for Result<T,E0>
  where E0 : Into<E>
{
  type R = Result<T,E>;
  fn cxm<S,MF>(self, mf : MF) -> Self::R
  where MF : FnOnce() -> S,
        S : ToOwned<Owned=String>,
        String : Borrow<S>
  {
    match self {
      Ok(y) => { Ok(y) }
      Err(e0) => {
        let e1 : E = e0.into();
        let e2 = e1.context(mf().to_owned());
        Err(e2)
      }
    }
  }
}
