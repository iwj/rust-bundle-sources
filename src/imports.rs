// Copyright 2020 Ian Jackson
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

pub use std::fs::{File,create_dir,remove_dir_all,remove_file,self};
pub use std::process::{Command,ExitStatus};
pub use std::io::ErrorKind::{NotFound,AlreadyExists};
pub use std::io::{self,Write,BufWriter};
pub use std::collections::BTreeMap;
pub use std::collections::HashMap;
pub use std::collections::hash_map::Entry::*;
pub use std::fmt::Debug;
pub use std::borrow::Borrow;
pub use std::ops::Deref;
pub use std::mem::replace;
pub use std::env::{self,VarError};
pub use std::os::unix::fs::symlink;
pub use std::cmp::Ordering;
pub use serde::{Serialize,Deserialize};

pub use cargo_metadata::{MetadataCommand,Package,Source};
pub use anyhow::{anyhow,Context};
pub use fehler::{throws,throw};
pub use thiserror::Error;

pub type E = anyhow::Error;

